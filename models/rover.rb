class Rover
  attr_accessor :pos_x, :pos_y, :direction

  def initialize(pos_x = 0, pos_y = 0, direction = :N, boundry_size_x = 5, boundry_size_y = 5)
    @pos_x = pos_x
    @pos_y = pos_y
    @direction = direction
    @boundry_size_x = boundry_size_x
    @boundry_size_y = boundry_size_y
  end

  def get_pos_x
    @pos_x
  end

  def get_pos_y
    @pos_y
  end

  def get_direction
    @direction
  end

  def get_position
    "#{@pos_x} #{@pos_y} #{@direction}"
  end

  # This method uses meta programming mapping,
  # this maps the commands that are sent to the rover directly to the commands method.
  def move_by command_data
    command_array = command_data.chars.to_a
    command_array.map{
        | command |
      send(command)
    }
  end

  private

  def L
    #puts "In L"
    new_direction = turn[@direction][:L]
    set_direction new_direction
  end

  def R
    #puts "In R"
    new_direction = turn[@direction][:R]
    set_direction new_direction
  end

  def M
    # puts "In M"
    # move by key and value
    # key = axis
    # value = Int val to move pos or neg
    # The same meta mapping/programming is used here to map the movement to the correct method my key
    move[@direction].map{
      |key, val|
        send(key.to_s, val)
    }
  end

  def turn
    {
        N: { L: :W, R: :E },
        E: { L: :N, R: :S },
        S: { L: :E, R: :W },
        W: { L: :S, R: :N }
    }
  end

  def move
    {
        N: { set_pos_y: +1 },
        S: { set_pos_y: -1 },
        E: { set_pos_x: +1 },
        W: { set_pos_x: -1 }
    }
  end

  # First check to see if the next move is within the boundary's of the Rover Platform (min 0 and Max defined)
  def set_pos_x pos_x
    new_pos_x = @pos_x + pos_x
    return if new_pos_x < 0 || new_pos_x > @boundry_size_x
    @pos_x = @pos_x + pos_x
  end

  # First check to see if the next move is within the boundary's of the Rover Platform (min 0 and Max defined)
  def set_pos_y pos_y
    new_pos_y = @pos_y + pos_y
    return if new_pos_y < 0 || new_pos_y > @boundry_size_y
    @pos_y = @pos_y + pos_y
  end

  def set_direction direction
    @direction = direction
  end

end