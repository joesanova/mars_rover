require_relative('models/rover_manager.rb')

puts "Please make sure the correct arguments are passed in, in this order: boundry_sie_x boundry_size_y rover_start_pos_x rover_start_pos_y rover_start_direction" if ARGV.length < 6
if ARGV.length == 6
  p ARGV
  boundary_size_x = ARGV[0].to_i
  boundary_size_y = ARGV[1].to_i
  rover_start_pos_x = ARGV[2].to_i
  rover_start_pos_y = ARGV[3].to_i
  rover_start_direction = ARGV[4].to_sym
  rover_commands = ARGV[5]

  mars_rovers = RoverManager.new(boundary_size_x, boundary_size_y)
  rover = mars_rovers.start_new_rover(rover_start_pos_x, rover_start_pos_y, rover_start_direction)

  puts "=-==-=-=-=-=- START POSITION =-=-=-=-=-=-=-=-=-=-="
  puts "#{mars_rovers.get_position_of rover}"
  puts "=-==-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-="

  rover.move_by rover_commands

  puts "=-==-=-=-=-=- END POSITION =-=-=-=-=-=-="
  puts "#{mars_rovers.get_position_of rover}"
  puts "=-==-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-="
end